﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequenceDemo : MonoBehaviour {

	public event System.Action OnDemoStarted;
	public event System.Action OnDemoFinished;

	[SerializeField]
	private Animator[] playerAnimators;

	[SerializeField]
	private Animator[] opponentAnimators;

	[SerializeField]
	private string animationState;

	[SerializeField]
	private float buttonAnimationInterval = 1f;

	public void Demo(IList<int> sequence)
	{
		StartCoroutine(DemoCoroutine(sequence));
	}

	private IEnumerator DemoCoroutine(IList<int> sequence)
	{
		OnDemoStarted?.Invoke();

		foreach (var num in sequence)
		{
			playerAnimators[num].Play(animationState);
            opponentAnimators[num].Play(animationState);
            yield return new WaitForSeconds(buttonAnimationInterval);
		}

		OnDemoFinished?.Invoke();
	}
}
