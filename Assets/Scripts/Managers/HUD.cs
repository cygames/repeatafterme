﻿using TMPro;
using UnityEngine;

public class HUD : MonoBehaviour {
	[SerializeField]
	private TextMeshProUGUI text;

	public void SetSequenceSizeText(string text)
	{
		this.text.SetText(text);
	}
}
