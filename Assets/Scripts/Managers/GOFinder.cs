﻿using UnityEngine;

public class GOFinder : MonoBehaviour
{
    // Setting up Singleton
    private static GOFinder instance;
    private GOFinder() { instance = this; }

    public static GameManager GameManager => instance.gameManager;
    public static SequenceGenerator SequenceGenerator => instance.sequenceGenerator;
    public static SequenceDemo SequenceDemo => instance.sequenceDemo;

    [Header("Overrides")]
    [SerializeField] private GameManager gameManager;
    [SerializeField] private SequenceGenerator sequenceGenerator;
    [SerializeField] private SequenceDemo sequenceDemo;

    private void Awake()
    {
        PopulateIfNull(gameManager, () => gameManager = FindObjectOfType<GameManager>());
        PopulateIfNull(sequenceGenerator, () => sequenceGenerator = FindObjectOfType<SequenceGenerator>());
        PopulateIfNull(sequenceDemo, () => sequenceDemo = FindObjectOfType<SequenceDemo>());
    }

    private void PopulateIfNull(object field, System.Action populateValue)
    {
        if (null == field)
        {
            populateValue();
        }
    }
}
