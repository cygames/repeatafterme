﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public int sequenceSize = 4;

    #region Events
    public System.Action<IList<int>> OnSequenceGenerated;
    public System.Action OnInputDisabled;
    public System.Action OnInputEnabled;
    public System.Action<bool> OnGameFinished;
    #endregion

    [SerializeField]
    private HUD hud;

    [SerializeField]
    private GameObject winnerOverlayPrefab;

    [SerializeField]
    private GameObject loserOverlayPrefab;

    [SerializeField]
    private PlayerController mainPlayer;
    private Transform playerPane;

    [SerializeField]
    private PlayerController secondaryPlayer;
    private Transform opponentPane;

    private IList<int> sequence;

    private GameObject winnerOverlay;
    private GameObject loserOverlay;

    private void OnEnable()
    {
        GOFinder.SequenceDemo.OnDemoFinished += EnableAfterDemo;

        mainPlayer.OnFinishedSequence += HandleFinishedSequence;
        secondaryPlayer.OnFinishedSequence += HandleFinishedSequence;
    }

    private void OnDisable()
    {
        GOFinder.SequenceDemo.OnDemoFinished -= EnableAfterDemo;

        mainPlayer.OnFinishedSequence -= HandleFinishedSequence;
        secondaryPlayer.OnFinishedSequence -= HandleFinishedSequence;
    }

    private void Start()
    {
        winnerOverlay = Instantiate(winnerOverlayPrefab);
        winnerOverlay.SetActive(false);

        loserOverlay = Instantiate(loserOverlayPrefab);
        loserOverlay.SetActive(false);

        playerPane = mainPlayer.transform.parent;
        opponentPane = secondaryPlayer.transform.parent;
        GenerateSequence();
    }

    public void ParseSequenceSizeSlider(Slider slider)
    {
        sequenceSize = (int) slider.value;
        hud.SetSequenceSizeText(sequenceSize.ToString());
    }

    public void GenerateSequence()
    {
        sequence = GOFinder.SequenceGenerator.GenerateSequence(sequenceSize);
        OnSequenceGenerated?.Invoke(sequence);
        Demo();
    }

    public void Demo()
    {
        Reset();
        OnInputDisabled?.Invoke();

        GOFinder.SequenceDemo.Demo(sequence);
    }

    private void Reset()
    {
        if (winnerOverlay.activeSelf)
        {
            winnerOverlay.SetActive(false);
        }

        if (loserOverlay.activeSelf)
        {
            loserOverlay.SetActive(false);
        }
    }

    private void EnableAfterDemo()
    {
        OnInputEnabled?.Invoke();
    }

    private void HandleFinishedSequence(bool isMainPlayer, float delay)
    {
        OnInputDisabled?.Invoke();
        OnGameFinished?.Invoke(isMainPlayer);

        if (isMainPlayer)
        {
            Debug.Log("Player has won!");
            winnerOverlay.transform.position = playerPane.position;
            loserOverlay.transform.position = opponentPane.position;
        }
        else
        {
            Debug.Log("Opponent has won!");
            winnerOverlay.transform.position = opponentPane.position;
            loserOverlay.transform.position = playerPane.position;
        }

        winnerOverlay.SetActive(true);
        loserOverlay.SetActive(true);
    }
}
