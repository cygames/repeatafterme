﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextSequenceEnabler : MonoBehaviour
{
    [SerializeField] private GameObject target;

    void OnEnable()
    {
        GOFinder.GameManager.OnSequenceGenerated += Disable;
        GOFinder.GameManager.OnGameFinished += Enable;
    }

    void OnDisable()
    {
        GOFinder.GameManager.OnSequenceGenerated -= Disable;
        GOFinder.GameManager.OnGameFinished -= Enable;
    }

    private void Enable(bool obj) => target.SetActive(true);

    private void Disable(IList<int> sequence) => target.SetActive(false);
}
