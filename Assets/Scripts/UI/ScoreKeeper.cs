﻿using UnityEngine;
using TMPro;

public class ScoreKeeper : MonoBehaviour
{
    [SerializeField] private bool isMainPlayer;

    private int score = 0;
    private TextMeshProUGUI textField;

    private void OnEnable()
    {
        GOFinder.GameManager.OnGameFinished += HandleGameFinished;
    }

    private void OnDisable()
    {
        GOFinder.GameManager.OnGameFinished -= HandleGameFinished;
    }

    private void Start()
    {
        textField = GetComponent<TextMeshProUGUI>();
        textField.SetText("0");
    }

    private void HandleGameFinished(bool mainPlayerWon)
    {
        if (this.isMainPlayer == mainPlayerWon)
        {
            score++;
            textField.SetText(score.ToString());
        }
    }
}
