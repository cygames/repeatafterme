﻿using System.Collections;
using UnityEngine;

public class Mover : MonoBehaviour
{
    [SerializeField] private float distance = 1920;
    [SerializeField] private float duration = 0.35f;
    [SerializeField] private float endApproximation = 0.01f;

    public void MoveRight()
    {
        Move(distance);
    }

    public void MoveLeft()
    {
        Move(-distance);
    }

    private void Move(float delta)
    {
        StartCoroutine(DoMove(delta));
    }

    private IEnumerator DoMove(float delta)
    {
        var startTime = Time.time;
        var minDuration = duration - endApproximation;
        var fraction = (Time.time - startTime) / duration;
        var startPos = transform.localPosition;
        var targetPos = startPos + new Vector3(delta, 0, 0);

        while (fraction < minDuration)
        {
            transform.localPosition = Vector3.Lerp(startPos, targetPos, fraction);
            fraction = (Time.time - startTime) / duration;
            yield return new WaitForEndOfFrame();
        }

        transform.localPosition = targetPos;
    }
}
