﻿using System.Collections.Generic;
using UnityEngine;

public class SequenceGenerator : MonoBehaviour {

	public float sameInARowChance = 0.1f;

	public IList<int> GenerateSequence(int size)
	{
		var sequence = new List<int>();

		var selection = Random.Range(0, 4);
		sequence.Add(selection);

		for (var i = 1; i < size; i++)
		{
			if (Random.value > sameInARowChance)
			{
				var nextSelection = Random.Range(0, 3);
				if (nextSelection < selection)
				{
					selection = nextSelection;
				}
				else
				{
					selection = nextSelection + 1;
				}
			}

			sequence.Add(selection);
		}

		return sequence;
	}

	//public void LogSequence(int size)
	//{
	//	var sequence = GenerateSequence(size);

	//	foreach (var num in sequence)
	//	{
	//		Debug.Log(num);
	//	}
	//}
}
