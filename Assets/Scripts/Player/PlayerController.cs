﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public System.Action<bool, float> OnFinishedSequence;

	[SerializeField]
	private bool isMainPlayer;

    private IList<int> sequence;
	private Queue<int> inputSequence;
	private Clickable[] buttons;
	private float startTime = -1;

	private void OnEnable()
    {
        var gameManager = GOFinder.GameManager;
        gameManager.OnSequenceGenerated += OnSequenceGenerated;
		gameManager.OnInputEnabled += OnInputEnabled;

        buttons = gameObject.GetComponentsInChildren<Clickable>();
		foreach(var button in buttons)
        {
			button.OnClicked += HandleButtonClicked;
        }
	}

	private void OnDisable()
    {
        var gameManager = GOFinder.GameManager;
        gameManager.OnSequenceGenerated -= OnSequenceGenerated;
		gameManager.OnInputEnabled -= OnInputEnabled;

		foreach (var button in buttons)
		{
			button.OnClicked -= HandleButtonClicked;
		}
	}

	private void OnSequenceGenerated(IList<int> sequence)
    {
        this.sequence = sequence;
		inputSequence = new Queue<int>();
	}

	private void HandleButtonClicked(bool ofMainPlayer, int buttonIndex)
	{
		inputSequence.Enqueue(buttonIndex);

		if (inputSequence.Count > sequence.Count)
		{
			inputSequence.Dequeue();
		}

		if (CheckSequence(inputSequence))
		{
			OnFinishedSequence?.Invoke(isMainPlayer, Time.time - startTime);
		}
	}

	private bool CheckSequence(Queue<int> inputSequence)
	{
		if (sequence.Count != inputSequence.Count)
		{
			return false;
		}

		var i = 0;
		foreach (var input in inputSequence)
		{
			if (input != sequence[i++])
			{
				return false;
			}
		}

		return true;
	}

	private void OnInputEnabled() => startTime = Time.time;
}
