﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Clickable : MonoBehaviour, IPointerDownHandler {

	public System.Action<bool, int> OnClicked;

	[SerializeField]
	private Animator pressedAnimator;

	[SerializeField]
	private string animationTrigger;

	[SerializeField]
	private int index;

	[SerializeField]
	private bool ofPlayer;

	private bool interactable = true;

	private void OnValidate()
    {
        // Expect that if animation trigger is set, then there should be an animator as well
        var animator = GetComponent<Animator>();
		if (null == animator ^ string.IsNullOrEmpty(animationTrigger))
        {
			throw new MissingComponentException("Animation trigger was set but no animator was attached, or vice versa. Check " + gameObject.name);
        }
	}
 
    private void OnEnable()
    {
        var gameManager = GOFinder.GameManager;
        gameManager.OnInputDisabled += OnInputDisabled;
		gameManager.OnInputEnabled += OnInputEnabled;
    }

    private void OnDisable()
    {
		var gameManager = GOFinder.GameManager;
		gameManager.OnInputDisabled -= OnInputDisabled;
		gameManager.OnInputEnabled -= OnInputEnabled;

	}

	private void Start()
    {
		pressedAnimator = GetComponent<Animator>();
    }

    public void OnPointerDown(PointerEventData eventData)
	{
		if (!interactable)
		{
			return;
		}

		if (!string.IsNullOrEmpty(animationTrigger))
		{
			pressedAnimator.SetTrigger(animationTrigger);
		}

        OnClicked?.Invoke(ofPlayer, index);
    }

	private void OnInputDisabled() => interactable = false;

	private void OnInputEnabled() => interactable = true;
}
