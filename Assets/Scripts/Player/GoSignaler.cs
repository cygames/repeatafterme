﻿using UnityEngine;

public class GoSignaler : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    private AnimationLifecycle animationLifecycle;

    private void OnEnable() => GOFinder.GameManager.OnInputEnabled += Signal;
    private void OnDisable() => GOFinder.GameManager.OnInputEnabled -= Signal;

    private void Signal()
    {
        animator?.gameObject.SetActive(true);
        animator?.SetTrigger("Play");

        animationLifecycle = animator.GetBehaviour<AnimationLifecycle>();
        animationLifecycle.OnStateExitAction += AnimationFinished;
    }

    private void AnimationFinished(AnimatorStateInfo stateInfo)
    {
        // We know that there is only one animation, thus no need to check the info
        animationLifecycle.OnStateExitAction -= AnimationFinished;
        animator?.gameObject.SetActive(false);
    }
}
