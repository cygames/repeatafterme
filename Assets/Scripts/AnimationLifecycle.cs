﻿using UnityEngine;

public class AnimationLifecycle : StateMachineBehaviour
{
    public System.Action<AnimatorStateInfo> OnStateEnterAction;
    public System.Action<AnimatorStateInfo> OnStateUpdateAction;
    public System.Action<AnimatorStateInfo> OnStateExitAction;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        OnStateEnterAction?.Invoke(stateInfo);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        OnStateUpdateAction?.Invoke(stateInfo);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        OnStateExitAction?.Invoke(stateInfo);
    }
}
